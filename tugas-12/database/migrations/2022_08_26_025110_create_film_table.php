<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilmTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('film', function (Blueprint $table) {
            $table->bigIncrements('film_id');
            $table->string('judul', 45);
            $table->string('ringkasan');
            $table->integer('tahun');
            $table->string('poster', 45);
            $table->unsignedBigInteger('genre_id');
            $table->foreign('genre_id')->references('genre_id')->on('genre');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('film');
    }
}
