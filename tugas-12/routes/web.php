<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;

Route::get('/', '\App\Http\Controllers\HomeController@index')->name('home');
Route::get('/form', '\App\Http\Controllers\AuthController@form')->name('form');
Route::post('/register', '\App\Http\Controllers\AuthController@register')->name('register');
Route::get('/welcome', '\App\Http\Controllers\AuthController@welcome')->name('welcome');
